

import UIKit
import CoreData
import MapKit
var filtered:[art] = []
var mapFilter:[art] = []
var currentTitle = ""


class MainViewController: UIViewController {
    
    var TableViewController: TableViewController?
    var mapViewController: MapViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(sortTableByLocation), name: NSNotification.Name(rawValue: "tableSort"), object: nil) // adds listenwe for table sorting method
        
        // Get the JSON
        if let url = URL(string: "https://cgi.csc.liv.ac.uk/~phil/Teaching/COMP327/artworksOnCampus/data.php?class=artworks2&lastUpdate=2017-11-01"){
            
            let session = URLSession.shared
            session.dataTask(with: url) { (data, response, err) in
                
                guard let jsonData = data else { return }
                do{
                    let decoder = JSONDecoder()
                    let artList = try decoder.decode(artwork.self, from: jsonData)
                    
                    pulledArt = Dictionary(grouping: artList.artworks2, by: {$0.location})
                    for (_, value) in pulledArt {
                    // store the JSON values
                        for currentArt in value {
                            let currentImage = currentArt.fileName
                            let imageURL = "https://cgi.csc.liv.ac.uk/~phil/Teaching/COMP327/artwork_images/" + currentImage
                            let encoding = imageURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            let url = URL(string: encoding!)
                            let data = try? Data(contentsOf: url!)
                            let downloadedImage = UIImage(data: data!)
                            let lat = currentArt.lat
                            let lon = currentArt.long
                            guard let latitude = Double(lat) else { return }
                            guard let longitude = Double(lon) else { return }
                            // adds the artwork object to an array ready for sorting
                            listOfArtWithDetails.append(ArtDetails(latitude: latitude, longitude: longitude, title: currentArt.title, artist: currentArt.artist, yearOfWork: currentArt.yearOfWork, information: currentArt.Information, locationBuilding: currentArt.location, image: downloadedImage!, locationNotes: currentArt.locationNotes))
                        }
                    }

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "mapLoad"), object: nil) // reload map
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableSort"), object: nil) // sort table
                    } catch let jsonErr {
                        print("Error decoding JSON", jsonErr)
                    }
                }
                .resume()
        }
        
    }
    
    @objc func sortTableByLocation(notification: NSNotification) {
        
        let unsorted = Dictionary(grouping: listOfArtWithDetails) { $0.locationBuilding } // group by building but not sort
        let sortedArt = unsorted.sorted { (aDic, bDic) -> Bool in
            return aDic.value[0].distance(to: myLocation!) < bDic.value[0].distance(to: myLocation!)
        }
        // put objects in sorted array with int key so easier to manipulate with table
        var i = 0
        for (key, value) in sortedArt {
            sortedArtWithDetails[key] = value
            arrayWithDetails[i] = value
            i = i + 1
        }
        self.TableViewController!.table.reloadData()
    }
    
    // segue preperation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        
        if segue.identifier == "to Details" { 
            for currentArtwork in sortedArtWithDetails[currentBuilding]! {
                if (currentArtwork.title == currentTitle){
                // pass these to the correct details screen
                    let secondViewController = segue.destination as! DetailsViewController
                    secondViewController.currentTitle = (currentArtwork.title)
                    secondViewController.currentArtist = (currentArtwork.artist)
                    secondViewController.currentYear = (currentArtwork.yearOfWork)
                    secondViewController.currentDetails = (currentArtwork.information)
                    secondViewController.currentLocation = (currentArtwork.locationNotes)
                    secondViewController.currentImage = (currentArtwork.image)
                }
            }
        }
        // reference child view controllers to use if needed
        if let locationController = destination as? TableViewController {
            TableViewController = locationController
        }
        
        if let mapController = destination as? MapViewController {
            mapViewController = mapController
        }
       
    }
        // Do any additional setup after loading the view.

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
