
import UIKit
import MapKit

// various structs used during pasing and sorting
var pulledArt = Dictionary<String, [art]>()

struct art: Decodable {
    let id: String
    let title: String
    let artist: String
    let yearOfWork: String
    let Information: String
    let lat: String
    let long: String
    let location: String
    let locationNotes: String
    let fileName: String
    let lastModified: String
    let enabled: String?
}


var listOfArtWithDetails = [ArtDetails]()
var sortedArtWithDetails = Dictionary<String, [ArtDetails]>()
var arrayWithDetails = [Int: [ArtDetails]]()

struct artwork: Decodable {
    let artworks2: [art]
}

// Main object for artwork, contains info, locations and images
class ArtDetails {
    let latitude: CLLocationDegrees
    let longitude: CLLocationDegrees
    let title: String
    let artist: String
    let yearOfWork: String
    let information: String
    let locationBuilding: String
    let image: UIImage
    let locationNotes: String
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees, title: String, artist: String, yearOfWork: String, information: String, locationBuilding: String, image: UIImage, locationNotes: String){
        self.latitude = latitude
        self.longitude = longitude
        self.title = title
        self.artist = artist
        self.yearOfWork = yearOfWork
        self.information = information
        self.locationBuilding = locationBuilding
        self.image = image
        self.locationNotes = locationNotes
        
    }
    var location: CLLocation { // returns the artwork location
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
    
    func distance(to location: CLLocation) -> CLLocationDistance { // returns the difference from inputted location
        return location.distance(from: self.location)
    }
}

