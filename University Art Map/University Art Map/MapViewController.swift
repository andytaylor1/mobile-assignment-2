import UIKit
import MapKit

let currentPlace = "" // set on clicking the table element
var currentBuilding = "" // set by clicking the table element or the annotation in map
var pinSelected = false // checks if pin has been clicked
var myLocation: CLLocation? // users location made accessable to all classes

class MapViewController: UIViewController, MKMapViewDelegate, UITableViewDelegate, CLLocationManagerDelegate {
    
    weak var tableViewController: UITableViewController?
    private var currentLocation: CLLocation?
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var map: MKMapView! // outlet for the map
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Asks for users location and sets
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        map.delegate = self
        // show the users location on map
        self.map.showsUserLocation = true
        
        // adds listener for loadmap method
        NotificationCenter.default.addObserver(self, selector: #selector(loadMap), name: NSNotification.Name(rawValue: "mapLoad"), object: nil)

    }
    
    @objc func loadMap(notification: NSNotification){
        let span = MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008)
        guard let userCoordinate = locationManager.location?.coordinate else {return}
        myLocation = locationManager.location
        let region = MKCoordinateRegion(center: userCoordinate, span: span)
        self.map.setRegion(region, animated: true) // Sets the initial map screen location
        
        for (name, artwork) in pulledArt { // Go through the buildingg, get location and add annotation
            let lat = artwork[0].lat
            let lon = artwork[0].long
            guard let latitude = Double(lat) else { return }
            guard let longitude = Double(lon) else { return }
            
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = name
            self.map.addAnnotation(annotation)
            
        }
        // triggers the sort to location nearest user
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableSort"), object: nil)
        // relads the table
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
    }
    
    // Method for if the user clicks on an annotation
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        pinSelected = true
        
        // ignores the tap on the user location as this crashes the app
        if (mapView.userLocation.title != view.annotation?.title){
            currentBuilding = ((view.annotation?.title!)!)
            mapFilter = pulledArt[currentBuilding]! // filters the table to selected building
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil) // reloads the table
        }
        
    }
    
    // method for if the user clicks away from an annotation
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        pinSelected = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil) // reloads the table
    }

}


