//
//  ViewController.swift
//  University Art Map
//
//  Created by Taylor, Andrew on 29/11/2018.
//  Copyright © 2018 Taylor, Andrew. All rights reserved.
//

import UIKit
import MapKit



class ViewController: UIViewController, MKMapViewDelegate {
    var places = [Dictionary<String, String>()]
    var TVC:TableViewController?
    
    override func prepare(for seque: UIStoryboardSegue, sender: Any?) {
        if (seque.identifier == "tableSeque")
        {
            TVC = (seque.destination as! TableViewController)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        places.append(["name":"Ashton Building", "lat": "53.406566", "lon": "-2.966531"])
        TVC?.table.reloadData()
    }
    

}

