

import UIKit

class TableViewController: UITableViewController, UISearchBarDelegate {
    @IBOutlet var table: UITableView!
    @IBOutlet weak var search: UISearchBar!
    var searchActive : Bool = false // for changing the mode of the table when search is active

    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        search.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil) // add listener for table reload
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async{
            self.table.reloadData()
        }
    }
    
    @objc func loadList(notification: NSNotification){
        DispatchQueue.main.async{
            self.table.reloadData()
        }
    }
    
    // Methods that control the search bar
    
    func searchBarTextDidBeginEditing(_ search: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ search: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ search: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ search: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var arrayConvert: [art] = []
    
        for (_, value) in pulledArt { // flatten 2d array to 1d array
            var i = 0
            while i < value.count {
                arrayConvert.append(value[i])
                i = i + 1
            }
    
        }
        // filter results by title artist or location
        filtered = arrayConvert.filter {$0.title.contains(searchText) || $0.artist.contains(searchText) || $0.location.contains(searchText)}
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.table.reloadData()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        
        // only one section if search is in use
        if (searchActive || pinSelected) {
            return 1
        }
        else {
            return arrayWithDetails.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // if annotation clicked
        if (pinSelected){
            search.isHidden = true // hides the search bar when pim is clicked as not needed
            return mapFilter.count
        }
        // if search is active
        else if (searchActive) {
            search.isHidden = false
            return filtered.count
            
        }
        // normal operation
        else {
            search.isHidden = false
            return (arrayWithDetails[section]?.count)!
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (pinSelected) {
            let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
            let locationObjects = Array(mapFilter)
            cell.textLabel?.text = locationObjects[indexPath.row].title
            cell.detailTextLabel?.text = locationObjects[indexPath.row].location
            return cell
        }
        else if (searchActive) {
            let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
            let locationObjects = Array(filtered)
            cell.textLabel?.text = locationObjects[indexPath.row].title
            cell.detailTextLabel?.text = locationObjects[indexPath.row].location
            return cell
        }
        else {
            let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
            //let locationObjects = Array(arrayWithDetails.values)[indexPath.section]
            cell.textLabel?.text = arrayWithDetails[indexPath.section]![indexPath.row].title
            cell.detailTextLabel?.text = arrayWithDetails[indexPath.section]![indexPath.row].artist
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (pinSelected) {
            return currentBuilding
        }
        else if (searchActive) { // clears the section title on search
            return  ""
        }
        else {
            return  arrayWithDetails[section]![0].locationBuilding
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (pinSelected){
            let parentViewContoller = self.parent
            currentBuilding = mapFilter[indexPath.row].location
            currentTitle = mapFilter[indexPath.row].title
            parentViewContoller?.performSegue(withIdentifier: "to Details", sender: parentViewContoller)
        }
        else if (searchActive) {
            let parentViewContoller = self.parent
            currentBuilding = filtered[indexPath.row].location
            currentTitle = filtered[indexPath.row].title
            parentViewContoller?.performSegue(withIdentifier: "to Details", sender: parentViewContoller)
        }
        else {
            let parentViewContoller = self.parent
            //let sectionBuilding = Array(sortedArtWithDetails.keys)
            //let sectionName = Array(sortedArtWithDetails.values)
            currentBuilding = arrayWithDetails[indexPath.section]![indexPath.row].locationBuilding
            currentTitle = arrayWithDetails[indexPath.section]![indexPath.row].title
            parentViewContoller?.performSegue(withIdentifier: "to Details", sender: parentViewContoller)
        }
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
