//
//  DetailsViewController.swift
//  University Art Map
//
//  Created by Taylor, Andrew on 14/12/2018.
//  Copyright © 2018 Taylor, Andrew. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var artist: UILabel!
    @IBOutlet weak var years: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var details: UITextView!
    
    
    
    var currentTitle = ""
    var currentArtist = ""
    var currentYear = ""
    var currentDetails = ""
    var currentLocation = ""
    var currentImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        artist.text = currentArtist // Set the fields to the local variables
        titleView.text = currentTitle
        years.text = currentYear
        location.text = currentLocation
        details.text = currentDetails
        imageView.contentMode = .scaleAspectFit
        imageView.image = currentImage
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
